<?php

/**
 * User  : Nikita
 * Date  : 10/29/15
 * E-Mail: esaverde228@gmail.com
 *
 * Akuma theme (theme://) stream wrapper class.
 *
 * Provides support for retrieve publicly accessible files from current theme directory
 */
class AkumaThemeStreamWrapper extends DrupalPublicStreamWrapper
{
    protected $theme_path;

    public function  __construct()
    {
        $this->theme_path = path_to_theme();
    }

    public function getThemePath(){
        return trim(path_to_theme(),'\\/');
    }

    public function getDirectoryPath()
    {
        return implode(DIRECTORY_SEPARATOR ,array(rtrim(DRUPAL_ROOT,'\\/'), $this->getThemePath()));
    }

    /**
     * Overrides getExternalUrl().
     *
     * Return the HTML URI of a theme file.
     */
    function getExternalUrl()
    {
        $target = drupal_encode_path(str_replace(array('\\\\','\\'), '/', $this->getTarget()));

        /**
         * TODO : Add Support for CDN
         */
        return url(str_replace('\\','/',$this->getThemePath()) . '/' . $target, array('absolute' => TRUE));
    }

    /**
     * Returns the canonical absolute path of the URI, if possible.
     *
     * @param string $uri
     *   (optional) The stream wrapper URI to be converted to a canonical
     *   absolute path. This may point to a directory or another type of file.
     *
     * @return string|false
     *   If $uri is not set, returns the canonical absolute path of the URI
     *   previously set by the DrupalStreamWrapperInterface::setUri() function.
     *   If $uri is set and valid for this class, returns its canonical absolute
     *   path, as determined by the realpath() function. If $uri is set but not
     *   valid, returns FALSE.
     */
    protected function getLocalPath($uri = null)
    {
        if (!isset($uri)) {
            $uri = $this->uri;
        }
        $path = $this->getDirectoryPath() . '/' . $this->getTarget($uri);
        $realpath = realpath($path);
        if (!$realpath) {
            // This file does not yet exist.
            $realpath = realpath(dirname($path)) . '/' . drupal_basename($path);
        }
        $directory = realpath($this->getDirectoryPath());
        if (!$realpath || !$directory || strpos($realpath, $directory) !== 0) {
            return false;
        }

        return $realpath;
    }
}