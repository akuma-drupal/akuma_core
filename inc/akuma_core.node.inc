<?php
/**
 * User  : Nikita.Makarov
 * Date  : 5/20/15
 * Time  : 7:41 AM
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Admin page callbacks for the Akuma Core module.
 */

/**
 * Menu callback; presents the node editing form.
 */
function akuma_node_type_change($node)
{
    $type_name = node_type_get_name($node);
    drupal_set_title(
      t('<em>Change type from @type</em>', array('@type' => $type_name, '@title' => $node->title)),
      PASS_THROUGH
    );

    return drupal_get_form('akuma_node_type_form', $node);
}

/**
 * Form builder; Configure settings.
 *
 * @ingroup forms
 *
 * @see     system_settings_form()
 */
function akuma_node_type_form($form, $form_state, $node)
{

    $types = node_type_get_types();
    $options = array();
    foreach ($types as $type) {
        $options[$type->type] = $type->name;
    }

    $form['new_type'] = array(
      '#type'          => 'select',
      '#title'         => t('Select new type'),
      '#default_value' => $node->type,
      '#options'       => $options,
    );
    // Clear the cache bins on submit.
    $form['#submit'][] = 'akuma_node_type_form_submit';

    return system_settings_form($form);
}

/**
 * Submit callback.
 */
function akuma_node_type_form_submit($form, &$form_state)
{
    $node = $form_state['build_info']['args'][0];
    if (!($form_state['values']['new_type'] == $node->type)) {
        $node->type = $form_state['values']['new_type'];
        node_save($node);
    }
    drupal_goto('node/' . $node->nid . '/edit');
}