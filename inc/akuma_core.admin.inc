<?php
/**
 * User  : Nikita.Makarov
 * Date  : 5/20/15
 * Time  : 7:41 AM
 * E-Mail: mesaverde228@gmail.com
 * @file
 * Admin page callbacks for the Akuma Core module.
 */

/**
 * Form builder; Configure settings.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function akuma_admin_settings_form($form, $form_state) {
    drupal_set_title(t('Akuma: Configuration'));

    // Simple checkbox settings.
    $form['metatags'] = array(
        '#type' => 'fieldset',
        '#title' => t('Metatags Options'),
    );
    $form['metatags']['shortlink_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable shortlink metatag'),
        '#default_value' => variable_get('shortlink_enabled', FALSE),
        '#description' => t('Uncheck this box to completely disable shortlink metatag from page source.'),
    );

    $form['metatags']['hreflang_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable hreflang metatag'),
      '#default_value' => variable_get('hreflang_enabled', TRUE),
      '#description' => t('Check this box to enable hreflang metatag from page source.'),
    );

    $form['theme_options'] = array(
        '#type' => 'fieldset',
        '#title' => t('Theme Options'),
    );

    $form['theme_options']['add_theme_class'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add theme class'),
        '#default_value' => variable_get('add_theme_class', TRUE),
        '#description' => t('Check this box to add theme class to "Body" tag.'),
    );

    $form['cache_control'] = array(
        '#type' => 'fieldset',
        '#title' => t('Cache Headers'),
    );
    $form['cache_control']['last_modified'] = array(
        '#type' => 'checkbox',
        '#title' => t('Override Last Modified'),
        '#default_value' => variable_get('last_modified', FALSE),
        '#description' => t('Check this box to override default "Last Modified".'),
    );
    // Clear the cache bins on submit.
    $form['#submit'][] = 'akuma_admin_settings_form_submit';

    return system_settings_form($form);
}

// Submit callback.
/**
 * STUB
 */
function akuma_admin_settings_form_submit($form, &$form_state) {

}