<?php
/**
 * User  : Nikita.Makarov
 * Date  : 6/22/15
 * Time  : 10:34 AM
 * E-Mail: mesaverde228@gmail.com
 */
if (!function_exists('in_array_r')) {
    /**
     * Search recursively value inside array and sub-arrays
     *
     * @param $needle
     * @param $haystack
     *
     * @return bool
     */
    function in_array_r($needle, $haystack)
    {
        $found = false;
        foreach ($haystack as $item) {
            if ($item === $needle) {
                $found = true;
                break;
            } elseif (is_array($item)) {
                $found = in_array_r($needle, $item);
                if ($found) {
                    break;
                }
            }
        }

        return $found;
    }
}

if (!function_exists('akuma_core_use_hreflang')) {
    /**
     * Displays hreflang for language and regional URLs
     */
    function akuma_core_use_hreflang()
    {

        $path = current_path();

        /** Skip Admin Paths */
        if (path_is_admin($path)) {
            return;
        }

        $langs = language_list();
        $translations = array();

        if (is_callable('i18n_node_i18n_translate_path')) {
            $translations = i18n_node_i18n_translate_path($path) ? : $translations;
        }
        if (is_callable('i18n_taxonomy_i18n_translate_path')) {
            $translations = i18n_taxonomy_i18n_translate_path($path) ? : $translations;
        }


        foreach ($langs as $key => $lang) {
            $path = isset($translations[$key]) ? $translations[$key]['href'] : (drupal_is_front_page()?'<front>':$path);
            drupal_add_html_head_link(
              array(
                'rel'      => 'alternate',
                'hreflang' => $key,
                'href'     => url($path, array('absolute' => true, 'language' => $lang)),
              ),
              true
            );
        }
    }
}

if (!function_exists('pre')) {
    /**
     * Similar to "var_dump"
     *
     * @param $_
     */
    function pre($_)
    {
        $args = func_get_args();
        if (count($args)) {
            foreach ($args as $arg) {
                echo "<pre>" . htmlentities(
                    (is_array($arg) || is_object($arg)) ? print_r($arg, true) : $arg
                  ) . '</pre>';
                @flush();
                @ob_flush();
            }

        }
    }
}
if (!function_exists('block_load_by_theme')) {
    /**
     * Loads Block Info by theme
     *
     * @param        $module
     * @param        $delta
     * @param string $theme
     *
     * @return mixed
     */
    function block_load_by_theme($module, $delta, $theme = 'akuma_mobile')
    {
        $block = db_query(
          'SELECT * FROM {block} WHERE module = :module AND delta = :delta AND theme = :theme',
          array(
            ':module' => $module,
            ':delta'  => $delta,
            ':theme'  => $theme
          )
        )->fetchObject();

        return $block;
    }
}

if (!function_exists('theme_include')) {
    function theme_include($what, $requre = false, $once = false)
    {
        return implode(
          DIRECTORY_SEPARATOR,
          array(
            rtrim(DRUPAL_ROOT, DIRECTORY_SEPARATOR),
            trim(path_to_theme(), DIRECTORY_SEPARATOR),
            trim($what, DIRECTORY_SEPARATOR)
          )
        );
    }
}
if (!function_exists('is_admin')) {
    function is_admin($account = null)
    {
        $role = user_role_load_by_name('Administrator');
        if ((is_object($role)) && (isset($role->rid))) {
            return user_has_role($role->rid, $account);
        }

        return false;
    }
}
if (!function_exists('akuma_core_last_modified')) {
    /**
     * Replaces last modified http header with node update date
     */
    function akuma_core_last_modified()
    {
        if (variable_get('page_cache_without_database')) {
            $cache_enabled = true;
        } else {
            $cache_enabled = variable_get('cache');
        }

        if (variable_get('last_modified', false) && $cache_enabled && drupal_page_get_cache(true)) {
            $query = $_GET['q'];
            $date = null;
            switch (true) {
                case(strpos($query, 'node') !== false):
                    list($tmp, $nid) = explode('/', $query);
                    unset($tmp);
                    if (!is_numeric($nid)) {
                        break;
                    }
                    $node = db_select('node', 'n')
                      ->fields('n')
                      ->condition('nid', $nid, '=')
                      ->execute()
                      ->fetchAssoc();
                    if (is_array($node)) {
                        if (isset($node['changed']) && ($node['changed'] > 0)) {
                            $date = gmdate(DATE_RFC1123, $node['changed'] * 1);
                        } elseif (isset($node['created'])) {
                            $date = @gmdate(DATE_RFC1123, $node['created'] * 1);
                        }
                    }
                    break;
                case (strpos($query, 'taxonomy/term')):
                    break;
            }
            if (isset($date) && !is_null($date)) {
                drupal_add_http_header('Last-Modified', $date, true);
            }

        }
    }
}
