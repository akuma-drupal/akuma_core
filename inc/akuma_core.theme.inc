<?php
/**
 * User  : Nikita.Makarov
 * Date  : 7/15/15
 * Time  : 7:41 AM
 * E-Mail: mesaverde228@gmail.com
 *
 * @file
 * Theme based hooks & functions.
 */

/**
 * @param array $registry
 */
function akuma_core_theme_registry_alter(array &$registry) {
    if (!isset($registry['breadcrumb']['function'])) {
        return;
    }
    $registry['breadcrumb']['function_old'] = $registry['breadcrumb']['function'];
    $registry['breadcrumb']['function'] = 'theme_akuma_core_breadcrumb';
}

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function theme_akuma_core_breadcrumb($variables)
{
    $registry = theme_get_registry();
    $function = isset($registry['breadcrumb']['function_old'])?$registry['breadcrumb']['function_old']:'theme_breadcrumb';

    if(!is_null($function)&& is_callable($function)){

        $output = call_user_func_array($function,func_get_args());
        return str_replace('<h2 class="element-invisible">' . t('You are here') . '</h2>','',$output);
    }

    /**
     * Failback
     */
    $breadcrumb = $variables['breadcrumb'];

    if (!empty($breadcrumb)) {
        return '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';
    }
}